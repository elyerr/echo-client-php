<?php
namespace Elyerr\EchoClient\Socket;

use ErrorException;
use WebSocket\Client;
use Ramsey\Uuid\Guid\Guid;
use WebSocket\BadUriException;
use WebSocket\TimeoutException;
use WebSocket\BadOpcodeException;
use WebSocket\ConnectionException;
use Illuminate\Support\Facades\Log;

trait Socket
{
    /**
     * Schema to send event
     * @param string $channel
     * @param string $event
     * @param string $message 
     * @return array
     */
    private function schema($channel, $event, $message)
    {
        return [
            'id' => Guid::uuid4(),
            'event' => $event,
            'channel' => $channel,
            'message' => $message
        ];
    }

    /**
     * Open connection with the websocket server 
     * @return  Client
     */
    public function openConnection()
    {
        $server = $this->config()->host;
        $port = $this->config()->port;
        $protocol = $this->config()->protocol;

        $context = stream_context_create();
        stream_context_set_option($context, 'ssl', 'verify_peer', $this->config()->verify_peer);
        stream_context_set_option($context, 'ssl', 'verify_peer_name', $this->config()->verify_peer_name);
        stream_context_set_option($context, 'ssl', 'allow_self_signed', true);

        $client = new Client("$protocol://$server:$port", [
            'context' => $context,
            'timeout' => 60,
            'headers' => [
                'Sec-WebSocket-Protocol' => 'soap',
                'origin' => $_SERVER['HTTP_HOST'],
                'Cookie' => $this->authorizationCookie(),
                'Authorization' => $this->getBearerToken(),
            ],
        ]);

        return $client;
    }

    /**
     * Get the cookie to auth user in the channel
     * @return string|null
     */
    private function authorizationCookie()
    {
        $cookie = $this->config()->cookie_name ? request()->cookie($this->config()->cookie_name) : request()->cookie(config('session.cookie'));
        return $cookie;
    }

    /**
     * Get the authorization bearer token
     * @return string|null
     */
    public function getBearerToken()
    {
        $cookie = request()->bearerToken();
        return str_contains('Bearer', $cookie) ? $cookie : "Bearer " . $cookie;
    }

    /**
     * Send event on privateChannel
     * @param mixed $event
     * @param mixed $message
     * @param mixed $channel
     * @param mixed $token
     * @return void
     */
    public function privateChannel($event, $message, $channel = null)
    {
        $channel = $channel ?: $this->config()->channel;

        $data = $this->schema("private-$channel", $event, $message);

        try {
            $this->sendEvent($data);

        } catch (ConnectionException $e) {
            Log::error($e);
        } catch (BadOpcodeException $e) {
            Log::error($e);
        } catch (BadUriException $e) {
            Log::error($e);
        } catch (TimeoutException $e) {
            Log::error($e);
        }

    }

    /**
     * Send event on publicChannel
     * @param mixed $event
     * @param mixed $message
     * @param mixed $channel
     * @param mixed $token
     * @return void
     */
    public function publicChannel($event, $message, $channel = null)
    {
        $channel = $channel ?: $this->config()->channel;

        $data = $this->schema("public-$channel", $event, $message);

        try {

            $this->sendEvent($data);

        } catch (ConnectionException $e) {
            Log::error($e);
        } catch (BadOpcodeException $e) {
            Log::error($e);
        } catch (BadUriException $e) {
            Log::error($e);
        } catch (TimeoutException $e) {
            Log::error($e);
        }
    }

    /**
     * Send the event
     * @param mixed $message
     * @return void
     */
    public function sendEvent($message)
    {
        $client = $this->openConnection();

        try {

            $client->send(json_encode($message));

        } catch (ConnectionException $e) {

        } finally {
            $client->close();
        }
    }

    /**
     * get the config file
     * @return mixed
     */
    public function config()
    {
        try {
            return json_decode(json_encode(require base_path('config/echo-client.php')));

        } catch (ErrorException $e) {

            return json_decode(json_encode(require __DIR__ . "/../../config/echo-client.php"));
        }
    }
}

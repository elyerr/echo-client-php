# Echo Client PHP
Libreria desarrollada para conectar con el servidor [Echo Server](https://gitlab.com/elyerr/echo-server) para poyectos desarrollados en php


### CONFIGURACION
Cualquier configuracion se realiza en el archo que se encuentra en la carpeta config, si estas usando laravel puedes publicar este archivo mediante el comando `php artisan vendor:publish`

- **host**: host del server echo server

- **port**: puerto del server echo server 
  
- **protocol**: protocolo puede ser `ws` o `wss`

- **channel**: nombre del canal, si se tienen diferentes canales se les puede pasar por parametro
 

### IMPLEMENTACION

 - Instalacion
    ```
    composer require elyerr/echo-client-php
    ```

 - Publicar el archivo de configurar 
    ```
    php artisan vendor:publish --tag=echo-client
    ```

 - Import el trait en la clase a usar los eventos
   ```
   use Elyerr\Echo\Client\PHP\Socket\Socket;

   class GlobalController extends Controller
   {
      use  Socket;

      public function __construct()
      { 
         //ejemplo eventos privados
          $this->privateChannel('CreateUser', "new user");

         //ejecmplo eventos publico
         $this->privateChannel('RemoveUser', "remove user");
      }
 
   }
   ```